import React, {Component} from "react";
import {config} from "../../config";

export default class Result extends Component {
	componentDidMount() {
		const data = this.props.data;

		if (data) {
			let storageData = {
				name: data.name,
				ico: data.ico
			};

			let storage = JSON.parse(localStorage.getItem(config.historyKey));
			if (!storage) {
				storage = [];
			}

			storage.forEach(function (values, index) {
				if (values.ico === storageData.ico) {
					storage.splice(index, 1);
				}
			});
			storage.unshift(storageData);

			if (storage.length > 10) {
				storage.splice(-1, 1);
			}

			localStorage.setItem(config.historyKey, JSON.stringify(storage));
		}
	}

	render() {
		let data = this.props.data;

		let address = data.address.street;
		if (data.address.houseNumber.length > 0) {
			address += ' ' + data.address.houseNumber;
		}
		if (data.address.referenceNumber.length > 0) {
			address += '/' + data.address.referenceNumber;
		}
		address += ', ' + data.address.village;
		if (data.address.villagePart.length > 0) {
			address += ' - ' + data.address.villagePart;
		}
		address += ', ' + data.address.district;

		return (
			<div className="card">
				<h5 className="card-header">{data.name}</h5>
				<div className="card-body">
					<dl className="row">
						<dd className="col-sm-3">Subjekt</dd>
						<dt className="col-sm-9">{data.name}</dt>
						<dd className="col-sm-3">IČ</dd>
						<dt className="col-sm-9">{data.ico}</dt>
						<dd className="col-sm-3">DIČ</dd>
						<dt className="col-sm-9">{
							data.dic !== null && data.dic.length > 0 ?
								data.dic
							: '---'
						}</dt>
						<dd className="col-sm-3">Adresa</dd>
						<dt className="col-sm-9">{address}</dt>
					</dl>

					<p>{data.legalForm.name}</p>
				</div>
			</div>
		);
	}
};
import React, {Component} from "react";

export default class Navbar extends Component {
	render() {
		return (
			<nav className="navbar navbar-dark bg-dark">
				<span className="navbar-brand mb-0 h1">ARES api</span>
			</nav>
		);
	}
};
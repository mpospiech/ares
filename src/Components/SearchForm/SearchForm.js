import React, {Component} from "react";
import axios from "axios";
import {config} from "../../config";

export default class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			ic: '',
			error: null
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value,
			error: null
		});

		this.props.setResult(null);
	}

	handleSubmit(event) {
		event.preventDefault();

		this.props.setLoader();

		axios.get(config.apiUrl + '/ares', {
			params: {
				ic: this.state.ic
			}
		})
			.then(res => {
				let response = res.data;

				if (response.status === 'ok') {
					this.props.setResult(response.data);
				} else {
					this.setState({error: response.data.message});
				}

				this.props.setLoader(false);
			})
			.catch(error => {
				this.setState({
					error: 'Při načítání dat došlo k chybě. Opakujte akci později'
				});

				this.props.setLoader(false);
			});

		this.setState({
			ic: ''
		});
	}

	render() {
		const title = 'Zadejte IČ subjektu';

		return (
			<div className="card">
				<h5 className="card-header">{title}</h5>
				<div className="card-body">
					<form onSubmit={this.handleSubmit} className="form-inline">
						<div className="form-group mx-sm-3">
							<label htmlFor="ic" className="sr-only">IČ</label>
							<input type="number" className="form-control" name="ic" placeholder="12345678" value={this.state.ic} onChange={this.handleChange} />
						</div>
						<input type="submit" className="btn btn-success" value="Získat data"/>
					</form>

					{
						this.state.error ?

							<div className="alert alert-danger mt-3">{this.state.error}</div>
						:null
					}
				</div>
			</div>
		);
	}
};
import React, {Component} from "react";
import {config} from "../../config";
import Axios from "axios";

export default class History extends Component{
	constructor(props) {
		super(props);
		this.handleOnClickClearHistory = this.handleOnClickClearHistory.bind(this);
	}


	handleOnClick(ico, event) {
		event.preventDefault();

		this.props.setLoader();

		Axios.get(config.apiUrl + '/ares', {
			params: {
				ic: ico
			}
		})
			.then(res => {
				let response = res.data;

				if (response.status === 'ok') {
					this.props.setResult(response.data);
				}

				this.props.setLoader(false);
			})
			.catch(error => {
				this.props.setLoader(false);
			});
	}

	handleOnClickClearHistory() {
		localStorage.clear();
		this.forceUpdate();
	}

	render() {
		let storageData = localStorage.getItem(config.historyKey);
		if (storageData) {
			storageData = JSON.parse(storageData);
		} else {
			storageData = [];
		}

		if (storageData.length > 0) {
			return (
				<div className="card mt-3">
					<h6 className="card-header bg-gradient-warning">Historie hledání</h6>
					<div className="card-body">
						<div className="list-group">
							{
								storageData.map((values, i) => {
									return <button onClick={(e) => this.handleOnClick(values.ico, e)} key={values.ico} className="list-group-item list-group-item-action">{values.ico}: <strong>{values.name}</strong></button>
								})
							}
						</div>
					</div>
					<div className="card-footer text-center">
						<button className="btn btn-danger btn-sm" onClick={this.handleOnClickClearHistory}>Smazat historii</button>
					</div>
				</div>
			);
		} else return null;
	}
}
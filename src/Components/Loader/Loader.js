import React, {Component} from "react";

export default class Loader extends Component {
	render() {
		if (this.props.enable) {
			return (
				<div
					style={{
						position: 'absolute',
						top: 0,
						left: 0,
						width: '100%',
						height: '100%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						textAlign: 'center',
						verticalAlign: 'middle',
						background: '#000',
						color: '#fff',
						opacity: 0.3
					}}
				>
					<div className="spinner-border" style={{width: '5rem', height: '5rem'}}><span className="sr-only">Loading</span></div>
				</div>
			);
		} else return null;
	}
};
import React, {Component} from 'react';
import Navbar from "./Components/Navbar/Navbar";
import SearchForm from "./Components/SearchForm/SearchForm";
import History from "./Components/History/History";
import Result from "./Components/Result/Result";
import Loader from "./Components/Loader/Loader";

export default class App extends Component{
	constructor(props) {
		super(props);
		this.state = {
			result: null,
			loader: false
		}

		this.setResult = this.setResult.bind(this);
		this.setLoader = this.setLoader.bind(this);
	}

	setResult(data) {
		this.setState({
			result: data
		});
	}

	setLoader(enable = true) {
		this.setState({
			loader: enable
		});
	}

	render() {
		const result = this.state.result;

		return (
			<div>
				<Navbar/>

				<div className="container-fluid pt-3">
					<div className="row">
						<div className="col-lg-3 col-sm-6">
							<SearchForm setResult={this.setResult} setLoader={this.setLoader}/>
							<History setResult={this.setResult} setLoader={this.setLoader}/>
						</div>
						{
							result !== null ?
								<div className="col-lg-4 col-sm-6">
									<Result data={result}/>
								</div>
								:null
						}
					</div>
				</div>

				<Loader enable={this.state.loader}/>
			</div>
		);
	}
}